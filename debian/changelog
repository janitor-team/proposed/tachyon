tachyon (0.99~b6+dsx-10) unstable; urgency=medium

  * Debianization:
    - d/copyright:
      - Copyright fields, refresh;
    - d/templates/control.in:
      - Rules-Requires-Root, introduce and set to no;
      - debhelper, bump to 12;
      - Standards Version, bump to 4.6.0 (no change);
    - d/rules:
      - SPACE (formerly EMPTY) definition, harden;
    - d/control, regenerated (Closes: #994291);
    - d/compat, discard.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 12 Oct 2021 13:05:33 +0000

tachyon (0.99~b6+dsx-9) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Change priority extra to optional.

  [ Jerome Benoit ]
  * Debianization:
    - debian/copyright:
      - Format field, secure;
      - Comment field, add;
      - Copyright fields, refresh;
    - debian/templates/control.in:
      - debhelper, bump to 11;
      - Standards Version, bump to 4.3.0 (no change);
      - Vcs-* fields, migration to salsa;
      - Priority fields, update;
    - debian/rules:
      - include pkg-info.mk;
      - percent target, discard --parallel option;
      - get-orig-source target, remove;
    - debian/clean, harden;
    - debian/source/*:
      - d/s/lintian-overrides, refresh;
      - d/s/options, remove;
    - debian/tachyon-doc.{doc-base,links}.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 26 Dec 2018 16:04:14 +0000

tachyon (0.99~b6+dsx-8) unstable; urgency=medium

  * Serious bug fix release (Closes: #855615) / Debianization:
    - d/rules:
      - link-doc, drop (See: #747141);
    - d/control:
      - -dev Depends fields, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 24 Feb 2017 13:51:03 +0000

tachyon (0.99~b6+dsx-7) unstable; urgency=medium

  * Serious bug fix release (Closes: #848131) / Debianization:
    - d/shlibs.local, indroduce (See: #831442).
  * Debianization:
    - d/templates/*:
      - d/t/control.in (resp., d/control), harden (resp., update);
    - d/patches/*:
      - d/p/upstream-rationalization-autotools.patch, refresh and harden;
      - d/p/debianization-bug-848363-ax_check_gl.patch, introduce;
      - Origin field, correct;
    - d/rules, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 16 Dec 2016 18:23:58 +0000

tachyon (0.99~b6+dsx-6) unstable; urgency=medium

  * FTBFS bug fix release (Closes: #837012):
    - configure.ac as provided for debianization, harden.
  * Debianization:
    - d/p/debianization.patch, see FTBFS bug fix above.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 13 Sep 2016 01:42:37 +0000

tachyon (0.99~b6+dsx-5) unstable; urgency=medium

  * Debianization:
    - debian/control:
      - Standards Version 3.9.8, bump;
      - description field appendum for serial platforms library, correct
        (thanks to Katsuhiko Nishimra <ktns.87@gmail.com> for noticing;
        Closes: #824134);
      - discard the debug symbols packages;
      - revisit;
    - debian/rules:
      - dpkg-buildflags, add hardening=+all;
      - discard the debug symbols material (see above);
      - symlink policy for documentation directories, set;
      - refresh;
    - debian/patches/upstream-lintian-spelling-error-silence.patch, introduce
      (and submit).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 01 Aug 2016 01:23:27 +0000

tachyon (0.99~b6+dsx-4) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 3.9.7, no changes needed.
  * Use HTTPS in the Vcs-* fields.
  * Use the list of openmpi architectures as provided by mpi-default-dev,
    instead of hardcoding our own list, and refresh debian/control.
  * Tight build-dep on mpi-default-dev (>= 1.3).

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 29 Feb 2016 13:37:56 +0000

tachyon (0.99~b6+dsx-3) unstable; urgency=medium

  * FTBFS bug fix release (Closes: #806115):
    - arch/indep build scheme, harden.
  * Debianization:
    - debian/rules, see FTBFS bug fix above;
    - debian/templates/control.in, Vcs-Browser field correction.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 02 Dec 2015 01:38:35 +0000

tachyon (0.99~b6+dsx-2) unstable; urgency=medium

  * Debianization:
    - debian/control, refresh;
    - debian/copyright, correct and refresh;
    - debian/rules, harden;
    - debian/patches/:
      - autotools chain:
        - libjpeg find and check, harden;
        - kfreebsd and hurd surpports, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 25 Jun 2015 16:58:12 +0000

tachyon (0.99~b6+dsx-1) unstable; urgency=medium

  * New upstrean release.
  * New maintainer (Closes: #764433).
  * Debianization:
    - debian/copyright:
      - in DEP-5 format, bump;
      - refresh;
    - debian/control:
      - debhelper build-dep to >= 9, bump;
      - Standards Version 3.9.6, bump;
      - Build-Depends field, refresh (Closes: #650601);
      - Vcs-* headers, provide (Closes: #697317);
    - debian/source/, format 3.0 (quilt), bump;
    - debian/watch, refresh;
    - debian/repack, repack script to clean up and gain weight;
    - debian/rules:
      - full, multi-flavour and multi-variant dh integration through
        a templates machinery;
      - get-orig-source uscan based target which downloads the currently
        packaged upstream tarball and repacks it;
      - default target which basically queries package status with uscan
        -- output in DEHS format;
    - debian/patches/:
      - patches in DEP-3 format;
      - harden pthreads support in GNU environment, thanks to
        Samuel Thibault <sthibault@debian.org> (Closes: #729182);
      - harden separate build either with or without OpenGL support;
      - format help display in view to employ help2man to generate manpages;
      - clean demo simple samples;
      - pre-rationalization cleanup;
      - merge the installed header into one header;
      - script-version, create by hand wrt `tachyon.h';
      - autotools build machinery, write from scratch to ease maintenance;
      - library versioning, introduce;
      - manual page generated via help2man;
    - debian/README.{source,Debian}, refresh;
    - build-arch/build-indep scheme, introduce;
    - gpg-signature check support, neutralize;
    - missing sample material gathered into a separated source tarball;
    - management of tools and library variants with update-alaternatives.
  * Provide an ad hoc script to shoot (and clean) the upstream scene samples,
    and an ad hoc Makefile to build (and clean) the upstream source examples.
  * Minor, enhancement, cosmetic patches submitted to the upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 15 Nov 2014 13:35:43 +0000

tachyon (0.99~b2+dfsg-0.4) unstable; urgency=low

  * Non-maintainer upload.
  * Add libtachyon-0.99 as a dependency of libtachyon-dev. Thanks to Ana Guerrero
    for finding this bug! (Closes: #675714)

 -- Mònica Ramírez Arceda <monica@debian.org>  Sun, 03 Jun 2012 00:27:58 +0200

tachyon (0.99~b2+dfsg-0.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fixed build failures on kfreebsd
    - disabling threading (Closes: 620999)
    - including sys/time.h in src/util.c

 -- Steffen Moeller <moeller@debian.org>  Thu, 21 Apr 2011 23:57:51 +0200

tachyon (0.99~b2+dfsg-0.2) unstable; urgency=low

  * Non-maintainer upload.
  * Moved TeX build requirements from Build-Depends-Indep
    to Build-Depends (Closes: #620068).

 -- Steffen Moeller <moeller@debian.org>  Tue, 05 Apr 2011 10:40:16 +0200

tachyon (0.99~b2+dfsg-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream version (Closes: #600938, #567997).
  * Reassignment of lib package to section lib (Closes: #490014).
  * Helped lintian cleanliness for BSD license warning.
  * No CDBS debian/control management.
  * Removed leading "../" from path in debian/patches/architecture.patch
  * Added LaTeX license to debian/copyright as requested by FTPadmins

 -- Steffen Moeller <moeller@debian.org>  Tue, 29 Mar 2011 13:37:53 +0200

tachyon (0.98~beta.dfsg-1) unstable; urgency=low

  * Set priority to optional.
  * Remove files generated by non-DFSG free latex2html.
  * Update to Standards-Version 3.8.0.
  * Create new linux-base-thr architecture for tachyon that doesn't assume
    x86 and incorrectly include -m32 in CFLAGS (Closes: #486849).

 -- Tim Abbott <tabbott@mit.edu>  Fri, 20 Jun 2008 11:24:50 -0400

tachyon (0.98~beta-1) unstable; urgency=low

  * Initial release (Closes: #480082).

 -- Tim Abbott <tabbott@mit.edu>  Fri, 06 Jun 2008 18:28:02 -0400
