#!/bin/bash

PACKAGE_NAME=tachyon

set -e
set -u

usage() {
	echo "Usage: repack --upstream-version <ver> <downloaded file>"
	exit 1
	}

if [ "$#" != "3" ]; then
	usage
fi
if [ "$1" != "--upstream-version" ]; then
	usage
fi
if [ ! -f "$3" ]; then
	if [ -n "$3" ]; then
		echo "$3 doesn't exist"
	fi
	usage
fi
UPSTREAM_VERSION="$2"
UPSTREAM_TARBALLZZ="$3"

DEBIAN_SUFFIX="+ds"
####DEBIAN_ENCAPSULATEDFOLDERNAME="${PACKAGE_NAME}-usrc"

DEBIAN_UVERSION=${UPSTREAM_VERSION}${DEBIAN_SUFFIX}
DEBIAN_ROOTFOLDERNAME="${PACKAGE_NAME}-${DEBIAN_UVERSION}.orig"
DEBIAN_TARBALLXZ="$(dirname $UPSTREAM_TARBALLZZ)/${PACKAGE_NAME}_${DEBIAN_UVERSION}.orig.tar.xz"

REPACK_TMPDIR=`mktemp -d ./repackXXXXXX`
REPACK_TMPDIR=$(readlink -f "$REPACK_TMPDIR")
trap "/bin/rm -rf \"$REPACK_TMPDIR\"" QUIT INT EXIT

message() {
	echo
	echo "-- -- $1"
	echo
	}

message "Repackaging $UPSTREAM_TARBALLZZ"

UPSTREAM_ROOTFOLDER="${REPACK_TMPDIR}/unpacked"
mkdir "${UPSTREAM_ROOTFOLDER}"
tar -C "${UPSTREAM_ROOTFOLDER}" -xf "${UPSTREAM_TARBALLZZ}" || unzip -d "${UPSTREAM_ROOTFOLDER}" "${UPSTREAM_TARBALLZZ}"
if [ `ls -1 "${UPSTREAM_ROOTFOLDER}" | wc -l` -eq 1 ]; then
	UPSTREAM_ROOTFOLDER="${UPSTREAM_ROOTFOLDER}/`ls -1 "${UPSTREAM_ROOTFOLDER}"`"
fi

DEBIAN_ROOTFOLDER="${REPACK_TMPDIR}/${DEBIAN_ROOTFOLDERNAME}"
## repack
####set -f
REPACK_WORKING_FOLDER=$(pwd)
cd "${UPSTREAM_ROOTFOLDER}"

####. "${REPACK_WORKING_FOLDER}/debian/repack.local"
## wipe out
find . -name '.#*' | xargs --no-run-if-empty rm --verbose --force
rm --verbose --force --recursive -- msvc
rm --verbose --force -- unix/Makefile.msvc
rm --verbose --force --recursive -- unix/CVS
rm --verbose --force --recursive -- scenes/CVS
rm --verbose --force --recursive -- msvc/tachyon/tachyon_ogl/CVS
rm --verbose --force --recursive -- msvc/tachyon/tachyon/CVS
rm --verbose --force --recursive -- msvc/tachyon/libtachyon/CVS
rm --verbose --force --recursive -- msvc/tachyon/CVS
rm --verbose --force --recursive -- msvc/CVS
rm --verbose --force --recursive -- demosrc/CVS
rm --verbose --force --recursive -- src/CVS
rm --verbose --force --recursive -- docs/CVS
rm --verbose --force -- docs/fc-english.def
rm --verbose --force -- docs/algorithmic.sty
rm --verbose --force -- docs/algorithm.sty
rm --verbose --force -- docs/datetime.sty
rm --verbose --force -- docs/fmtcount.sty
rm --verbose --force -- docs/lstmisc.sty
rm --verbose --force -- docs/lstlang3.sty
rm --verbose --force -- docs/lstlang2.sty
rm --verbose --force -- docs/lstlang1.sty
rm --verbose --force -- docs/lstdoc.sty
rm --verbose --force -- docs/listings.sty
rm --verbose --force -- docs/lineno.sty
rmdir --verbose --ignore-fail-on-non-empty -- compile
## clean up
rm --verbose --force --recursive -- docs/tachyon
rm --verbose --force -- docs/tachyon.{aux,ind,toc,idx,log,ilg}
rm --verbose --force -- docs/tachyon.{dvi,ps,pdf}
rm --verbose --force -- docs/tachyon.html.tar.gz
## remove executable bit for source files
chmod --changes a-x src/hash.c
chmod --changes a-x src/hash.h
chmod --changes a-x src/pngfile.h
chmod --changes a-x demosrc/spaceball.c
chmod --changes a-x demosrc/trackball.c
chmod --changes a-x demosrc/trackball.h
chmod --changes a-x scenes/imaps/*.ppm
chmod --changes a-x scenes/imaps/*.tga
chmod --changes a-x docs/pdfdraftcopy.sty
#:

cd ${REPACK_WORKING_FOLDER}
set +f
## end
####if [ -n "${DEBIAN_ENCAPSULATEDFOLDERNAME}" ]; then
####	mkdir "${DEBIAN_ROOTFOLDER}"
####	mv "${UPSTREAM_ROOTFOLDER}" "${DEBIAN_ROOTFOLDER}/${DEBIAN_ENCAPSULATEDFOLDERNAME}"
####else
	mv "${UPSTREAM_ROOTFOLDER}" "${DEBIAN_ROOTFOLDER}"
####fi

REPACK_TARBALL="${REPACK_TMPDIR}/repacked.tar"
REPACK_TARBALLXZ="${REPACK_TARBALL}.xz"
( cd "${REPACK_TMPDIR}" && \
	find -L "${DEBIAN_ROOTFOLDERNAME}" -xdev -type f -print | sort | \
	tar -T-  --owner=root --group=root --mode=a+rX --create --file "${REPACK_TARBALL}" \
	)
xz -9e < "${REPACK_TARBALL}" > "${REPACK_TARBALLXZ}"
mv "${REPACK_TARBALLXZ}" "${DEBIAN_TARBALLXZ}"

message "Testing ${DEBIAN_TARBALLXZ}"

xz --verbose --test "${DEBIAN_TARBALLXZ}"

message "Printing information about ${DEBIAN_TARBALLXZ}"

xz --verbose --list "${DEBIAN_TARBALLXZ}"

message "Quitting"

##
## eos
