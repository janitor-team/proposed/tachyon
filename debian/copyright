Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tachyon
Upstream-Contact: John E. Stone <johns@ks.uiuc.edu>
Source: http://jedi.ks.uiuc.edu/~johns/raytracer/
Comment:
 The upstream source tarball is repacked to drop off some substantial weight
 by wiping out included third-party material and to add some extra material
 needed for documentation and as samples.

Files: *
Copyright:
 1994-2021 John E. Stone <johns@ks.uiuc.edu>
License: BSD-3-clause

Files: demosrc/trackball.*
Copyright:
 1993, 1994 Silicon Graphics, Inc.
License: SGI-OpenGL

Files: docs/pdfdraftcopy.sty
Copyright:
 2003 C. V. Radhakrishnan <cvr@river-valley.com>
      C. V. Rajagopal
License: LPPL-1.0+

Files: pkgextra/virtual-reality/*
Copyright:
 1994 Avalon
License: public-domain-vr
 These files are public domain and come with NO WARRANTY of any kind.
Comment:
 These data files belong to the Avalon 3D Graphics Collection, a collection of public
 domain 3D models distributed by Avalon; Avalon was bought by Viewpoint. They appeared
 to be available at now down web site http://avalon.viewpoint.com/. They were grabbed
 as-is by hand from ftp://ftp6.de.freebsd.org/pub/sci/virtual-reality/.

Files: pkgextra/volpack/*
Copyright:
 1994 Phil Lacrout <volpack@graphics.stanford.edu>
License: public-domain-vp
 These files are public domain and come with NO WARRANTY of any kind.
Comment:
 These data files are part of the official VolPack rendering library distribution,
 which was available from the Stanford Computer Graphics Laboratory ftp site
 graphics.stanford.edu in pub/volpack/. They were fetched as-is by hand from
 ftp://ftp.u-aizu.ac.jp/pub/graphics/CG/raytracers/graphics.stanford.edu/volpack/.

Files: pkgextra/volpack/data/brain/*.den.Z
Copyright:
 1994 Phil Lacrout <volpack@graphics.stanford.edu>
License: public-domain-vp-chvrtd
 These data files constains volume data derived from an MR scan of a human head;
 the data are in .den format and has been compressed.
 .
 The original MR scan is from the Chapel Hill Volume Rendering Test Dataset,
 Volume I, formally available from <ftp://omicron.cs.unc.edu/pub/softlab/CHVRTD/volI/>.
 The data was taken on the Siemens Magnetom and provided courtesy of Siemens Medical
 Systems, Inc., Iselin, NJ. They may be redistributed provided that the above
 information about the source of the data is included.
 .
 The original data and the complete updated text of the Announcing of Chapel Hill
 Volume Rendering Test Data Set, are available at the time of writing from
 <http://graphics.stanford.edu/data/voldata/>.

Files: debian/*
Copyright:
 2014-2021 Jerome Benoit <calculus@rezozer.net>
 2008 Tim Abbott <tabbott@mit.edu>
License: GPL-3+
Comment:
 This package was originally debianized by Tim Abbott <tabbott@mit.edu>.

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: SGI-OpenGL
 ALL RIGHTS RESERVED
 Permission to use, copy, modify, and distribute this software for
 any purpose and without fee is hereby granted, provided that the above
 copyright notice appear in all copies and that both the copyright notice
 and this permission notice appear in supporting documentation, and that
 the name of Silicon Graphics, Inc. not be used in advertising
 or publicity pertaining to distribution of the software without specific,
 written prior permission.
 .
 THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU "AS-IS"
 AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE,
 INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR
 FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL SILICON
 GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DIRECT,
 SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY
 KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,
 LOSS OF PROFIT, LOSS OF USE, SAVINGS OR REVENUE, OR THE CLAIMS OF
 THIRD PARTIES, WHETHER OR NOT SILICON GRAPHICS, INC.  HAS BEEN
 ADVISED OF THE POSSIBILITY OF SUCH LOSS, HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE
 POSSESSION, USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 US Government Users Restricted Rights
 Use, duplication, or disclosure by the Government is subject to
 restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
 (c)(1)(ii) of the Rights in Technical Data and Computer Software
 clause at DFARS 252.227-7013 and/or in similar or successor
 clauses in the FAR or the DOD or NASA FAR Supplement.
 Unpublished-- rights reserved under the copyright laws of the
 United States.  Contractor/manufacturer is Silicon Graphics,
 Inc., 2011 N.  Shoreline Blvd., Mountain View, CA 94039-7311.
 .
 OpenGL(TM) is a trademark of Silicon Graphics, Inc.

License: LPPL-1.0+
 This program can redistributed and/or modified under the terms
 of the LaTeX Project Public License Distributed from CTAN
 archives in directory macros/latex/base/lppl.txt; either
 version 1 of the License, or (at your option) any later version.
 .
 You should have received a copy of the LaTeX Project Public License
 along with this package. If not, see
 <http://www.latex-project.org/lppl/lppl-1-0.txt>.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".
